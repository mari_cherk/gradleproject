package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.event.KeyEvent;

public class LoginTest {

    WebDriver driver;
    WebDriverWait wait;

    @BeforeTest
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        driver.get("http://demo.hospitalrun.io/#/login");
        driver.manage().window().maximize();
    }

    @Test
    public void loginWithValidData() throws AWTException {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("identification")));
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_TAB);
        robot.keyPress(KeyEvent.VK_ENTER);
        driver.findElement(By.id("identification")).sendKeys("hr.doctor@hospitalrun.io");
        driver.findElement(By.id("password")).sendKeys("HRt3st12");
        driver.findElement(By.xpath("//*[@type='submit']")).click();
        wait.until(ExpectedConditions.urlContains("http://demo.hospitalrun.io/#/patients"));
        Assert.assertEquals(driver.getCurrentUrl(), "http://demo.hospitalrun.io/#/patients", "Wrong URL");
    }

    @AfterTest
    public void quitBrowser() {
        driver.quit();
    }
}
